
#ifdef __AVR__

#ifndef _ATMEGA_TIMERS_H_
#define _ATMEGA_TIMERS_H_

#include <avr/interrupt.h>

class AtMegaTimer
{
protected:
	volatile unsigned long msecs;
	void (*func)();
	volatile unsigned long count;
	volatile char overflowing;

public:
	AtMegaTimer(){};
	virtual void set(unsigned long ms, void (*f)());
	virtual void start();
	virtual void stop();
};

class AtMegaTimer1 : public AtMegaTimer
{
public:
	volatile unsigned int tcnt1;
	void _overflow();
	AtMegaTimer1(){};
	void set(unsigned long ms, void (*f)());
	void setMicros(unsigned long us, void (*f)());	
	void start();
	void stop();
};

class AtMegaTimer2 : public AtMegaTimer
{
public:
	volatile unsigned int tcnt2;
	void set(unsigned long ms, void (*f)());
	void setMicros(unsigned long us, void (*f)());	
	void start();
	void stop();
	void _overflow();
};

class AtMegaTimer3 : public AtMegaTimer
{
public:
	volatile unsigned int tcnt3;
	void _overflow();
	AtMegaTimer3(){};
	void set(unsigned long ms, void (*f)());
	void setMicros(unsigned long us, void (*f)());	
	void start();
	void stop();
};

#if defined(__AVR_ATmega2560__)

class AtMegaTimer4 : public AtMegaTimer
{
public:
	volatile unsigned int tcnt4;
	void _overflow();
	AtMegaTimer4(){};
	void set(unsigned long ms, void (*f)());
	void setMicros(unsigned long us, void (*f)());	
	void start();
	void stop();
};

class AtMegaTimer5 : public AtMegaTimer
{
public:
	volatile unsigned int tcnt5;
	void _overflow();
	AtMegaTimer5(){};
	void set(unsigned long ms, void (*f)());
	void setMicros(unsigned long us, void (*f)());	
	void start();
	void stop();
};

extern AtMegaTimer4 atmegaTimer4;
extern AtMegaTimer5 atmegaTimer5;

#endif

extern AtMegaTimer1 atmegaTimer1;
extern AtMegaTimer2 atmegaTimer2;
extern AtMegaTimer3 atmegaTimer3;

#endif

#endif
