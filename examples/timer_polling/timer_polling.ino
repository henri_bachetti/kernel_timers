
/*
   Timer callbacks demo
   - 1 timer for inter character reception
   - 1 timer for inter frame reception

   Enter 2 words starting with $ and ending with *.
   You can enter words character by character (inter-character timeout is: 2s).
   Timeout between words is: 5s.
*/

#include <kernel_timers.h>

#define CHAR_TIMEOUT          (HZ*2)
#define FRAME_TIMEOUT         (HZ*5)

#define MAX_BUF             20
#define NMESSAGES           2
#define STATE_IDLE          0
#define STATE_STX_RECEIVED  1

char buf[20];
int buf_index;
int nmessages;
int state;

struct timer_list char_timer;
struct timer_list frame_timer;

void char_timeout(unsigned long data)
{
  mod_timer(&char_timer, 0);
  state = STATE_IDLE;
  buf_index = 0;
  Serial.println("char timeout ");
}

void frame_timeout(unsigned long data)
{
  mod_timer(&char_timer, 0);
  mod_timer(&frame_timer, 0);
  state = STATE_IDLE;
  buf_index = 0;
  nmessages = 0;
  Serial.println("frame timeout ");
}

void setup()
{
  Serial.begin(115200);
  // initialize timers system with TIMER1
  timer_init(1);
  init_timer(&char_timer);
  init_timer(&frame_timer);
  add_timer(&char_timer);
  add_timer(&frame_timer);
  Serial.print("Enter "); Serial.print(NMESSAGES); Serial.println(" words starting with $ and ending with *");
  Serial.print("You can enter words character by character (inter-character timeout is: ");
  Serial.print(CHAR_TIMEOUT * TICK); Serial.println(" ms)");
  Serial.print("Timeout between words is: ");
  Serial.print(FRAME_TIMEOUT * TICK); Serial.println(" ms\n");
}

void loop()
{
  if (char_timer.expires != 0 && char_timer.expires == jiffies) {
    char_timeout(0);
  }
  if (frame_timer.expires != 0  && frame_timer.expires == jiffies) {
    frame_timeout(0);
  }
  if (Serial.available()) {
    char c = Serial.read();
    switch (state) {
      case STATE_IDLE:
        if (c == '$') {
          mod_timer(&char_timer, jiffies + CHAR_TIMEOUT);
          state = STATE_STX_RECEIVED;
        }
        break;
      case STATE_STX_RECEIVED:
        switch (c) {
          case '*':
            mod_timer(&char_timer, 0);
            mod_timer(&frame_timer, jiffies + FRAME_TIMEOUT);
            Serial.print("RECEIVED: ");
            Serial.println(buf);
            nmessages++;
            buf_index = 0;
            state = STATE_IDLE;
            if (nmessages == NMESSAGES) {
              mod_timer(&frame_timer, 0);
              Serial.print("OK: "); Serial.print(nmessages); Serial.println(" words were received in time");
              nmessages = 0;
            }
            break;
          default:
            mod_timer(&char_timer, jiffies + CHAR_TIMEOUT);
            if (buf_index < MAX_BUF) {
              buf[buf_index++] = c;
            }
        }
        break;
      default:
        break;
    }
  }
}


