
/*
  This work is a merge of the followings :
  MsTimer1 MsTimer3 MsTimer4 MsTimer4 MsTimer5 by Tony Tan
*/

#ifdef __AVR__

#include "Arduino.h"
#include "atmega_timers.h"

void AtMegaTimer1::set(unsigned long ms, void (*f)()) {
	float prescaler = 0.0;
	
	TIFR1 = 0xff;
	TIMSK1 = 0;
	TCCR1A &= ~((1<<WGM11) | (1<<WGM10));
	TCCR1B &= ~((1<<WGM12) | (1<<WGM13));
	if (F_CPU < 1000000UL) {	// prescaler set to 8
		TCCR1B &= ~((1<<CS12) | (1<<CS10));
		TCCR1B |= (1<<CS11);
		prescaler = 8.0;
	}
	else {//if ((F_CPU >= 1000000UL) && (F_CPU <= 16000000UL)) 
		// prescaler set to 64
		TCCR1B &= ~(1<<CS12);
		TCCR1B |= (1<<CS11) | (1<<CS10);
		prescaler = 64.0;
	} 	
  tcnt1 = 65536U - (int)((float)F_CPU * 0.001 / prescaler);
	if(ms == 0) {
		msecs = 1;
	}
	else {
		msecs = ms;
	}
	func = f;
}

void AtMegaTimer1::setMicros(unsigned long us, void (*f)()) {
	float prescaler = 0.0;
	
	TIMSK1 = 0;
	TCCR1A &= ~((1<<WGM11) | (1<<WGM10));
	TCCR1B &= ~((1<<WGM12) | (1<<WGM13));
	//prescaler set to 1
	TCCR1B &= ~((1<<CS12) | (1<<CS11));
	TCCR1B |= (1<<CS10);
	prescaler = 1.0;
	if(us > 4095u)
		us = 4095u;
	tcnt1 = 65536U - (int)((float)F_CPU * 0.000001 * us / prescaler);
	msecs = 1;
	func = f;
}

void AtMegaTimer1::start() {
	count = 0;
	overflowing = 0;
	TCNT1H = tcnt1 >> 8;
	TCNT1L = tcnt1 & 0xff;
	TIFR1 |= _BV(TOV1);
	TIMSK1 |= _BV(TOIE1);
}

void AtMegaTimer1::stop() {
	TIMSK1 &= ~(_BV(TOIE1));
}

void AtMegaTimer1::_overflow() {
	count += 1;
	if (count >= msecs && !overflowing) {
		overflowing = 1;
		count = 0;
		func();
		overflowing = 0;
	}
}

#if defined(__AVR_ATmega2560__)
ISR(_VECTOR(20)) {
#else
ISR(_VECTOR(13)) {
#endif
	TCNT1H = atmegaTimer1.tcnt1 >> 8;
	TCNT1L = atmegaTimer1.tcnt1 & 0xff;
	TIFR1 |= _BV(TOV1);
	atmegaTimer1._overflow();
}

void AtMegaTimer2::set(unsigned long ms, void (*f)()) {
	float prescaler = 0.0;
	
  TIMSK2 &= ~(1<<TOIE2);
	TCCR2A &= ~((1<<WGM21) | (1<<WGM20));
	TCCR2B &= ~(1<<WGM22);
	ASSR &= ~(1<<AS2);
	TIMSK2 &= ~(1<<OCIE2A);
	
	if (F_CPU < 1000000UL) {	// prescaler set to 8
		TCCR2B |= (1<<CS21);
		TCCR2B &= ~((1<<CS22) | (1<<CS20));
    prescaler = 8.0;
  }
	else {//if ((F_CPU >= 1000000UL) && (F_CPU <= 16000000UL)) 
		// prescaler set to 64
		TCCR2B |= (1<<CS22);
		TCCR2B &= ~((1<<CS21) | (1<<CS20));
		prescaler = 64.0;
	}
	tcnt2 = 256 - (int)((float)F_CPU * 0.001 / prescaler);
	if (ms == 0) {
    msecs = 1;
  }
	else {
    msecs = ms;
  }
	func = f;
}

void AtMegaTimer2::start() {
	count = 0;
	overflowing = 0;
	TCNT2 = tcnt2;
	TIMSK2 |= (1<<TOIE2);
}

void AtMegaTimer2::stop() {
	TIMSK2 &= ~(1<<TOIE2);
}

void AtMegaTimer2::_overflow() {
	count += 1;
	if (count >= msecs && !overflowing) {
		overflowing = 1;
		count = count - msecs;
		(*func)();
		overflowing = 0;
	}
}

ISR(TIMER2_OVF_vect) {
	TCNT2 = atmegaTimer2.tcnt2;
	atmegaTimer2._overflow();
}

#if defined(__AVR_ATmega2560__)

void AtMegaTimer3::set(unsigned long ms, void (*f)()) {
	float prescaler = 0.0;
	
	TIFR3 = 0xff;
	TIMSK3 = 0;
	TCCR3A &= ~((1<<WGM31) | (1<<WGM30));
	TCCR3B &= ~((1<<WGM32) | (1 << WGM33));
	if (F_CPU < 1000000UL) {	// prescaler set to 8
		TCCR3B &= ~((1<<CS32) | (1<<CS30));
		TCCR3B |= (1<<CS31);
		prescaler = 8.0;
	}
	else {//if ((F_CPU >= 1000000UL) && (F_CPU <= 16000000UL)) 
		// prescaler set to 64
		TCCR3B &= ~(1<<CS32);
		TCCR3B |= (1<<CS31) | (1<<CS30);
		prescaler = 64.0;
	} 	
	tcnt3 = 65536U - (int)((float)F_CPU * 0.001 / prescaler);
	if(ms == 0)	{
		msecs = 1;
	}
	else {
		msecs = ms;
	}
	func = f;
}

void AtMegaTimer3::setMicros(unsigned long us, void (*f)()) {
	float prescaler = 0.0;
	
	TIMSK3 = 0;
	TCCR3A &= ~((1<<WGM31) | (1<<WGM30));
	TCCR3B &= ~((1<<WGM32) | (1 << WGM33));
	
	//prescaler set to 1
	TCCR3B &= ~((1<<CS32) | (1<<CS31));
	TCCR3B |= (1<<CS30);
	prescaler = 1.0;
	if(us > 4095u)
		us = 4095u;
	tcnt3 = 65536U - (int)((float)F_CPU * 0.000001 * us / prescaler);
	msecs = 1;
	func = f;
}

void AtMegaTimer3::start() {
	count = 0;
	overflowing = 0;
	TCNT3H = tcnt3 >> 8;
	TCNT3L = tcnt3 & 0xff;
	TIFR3 |= _BV(TOV3);
	TIMSK3 |= _BV(TOIE3);
}

void AtMegaTimer3::stop() {
	TIMSK3 &= ~(_BV(TOIE3));
}

void AtMegaTimer3::_overflow() {
	count += 1;
	if (count >= msecs && !overflowing) {
		overflowing = 1;
		count = 0;
		func();
		overflowing = 0;
	}
}

#if defined(__AVR_ATmega2560__)
ISR(_VECTOR(35)) {
#else
ISR(_VECTOR(29)) {
#endif
	TCNT3H = atmegaTimer3.tcnt3 >> 8;
	TCNT3L = atmegaTimer3.tcnt3 & 0xff;
	TIFR3 |= _BV(TOV3);
	atmegaTimer3._overflow();
}

void AtMegaTimer4::set(unsigned long ms, void (*f)()) {
	float prescaler = 0.0;
	
	TIFR4 = 0xff;
	TIMSK4 = 0;
	TCCR4A &= ~((1<<WGM41) | (1<<WGM40));
	TCCR4B &= ~((1<<WGM42) | (1 << WGM43));
	
	if (F_CPU < 1000000UL) {	// prescaler set to 8
		TCCR4B &= ~((1<<CS42) | (1<<CS40));
		TCCR4B |= (1<<CS41);
		prescaler = 8.0;
	}
	else {  //if ((F_CPU >= 1000000UL) && (F_CPU <= 16000000UL)) prescaler set to 64
		TCCR4B &= ~(1<<CS42);
		TCCR4B |= (1<<CS41) | (1<<CS40);
		prescaler = 64.0;
	} 	
	tcnt4 = 65536U - (int)((float)F_CPU * 0.001 / prescaler);
	if(ms == 0)	{
		msecs = 1;
	}
	else {
		msecs = ms;
	}
	func = f;
}

void AtMegaTimer4::setMicros(unsigned long us, void (*f)()) {
	float prescaler = 0.0;
	
	TIMSK4 = 0;
	TCCR4A &= ~((1<<WGM41) | (1<<WGM40));
	TCCR4B &= ~((1<<WGM42) | (1 << WGM43));
	//prescaler set to 1
	TCCR4B &= ~((1<<CS42) | (1<<CS41));
	TCCR4B |= (1<<CS40);
	prescaler = 1.0;
	if(us > 4095u)
		us = 4095u;
	tcnt4 = 65536U - (int)((float)F_CPU * 0.000001 * us / prescaler);
	msecs = 1;
	func = f;
}

void AtMegaTimer4::start() {
	count = 0;
	overflowing = 0;

	TCNT4H = tcnt4 >> 8;
	TCNT4L = tcnt4 & 0xff;
	TIFR4 |= _BV(TOV4);
	TIMSK4 |= _BV(TOIE4);
}

void AtMegaTimer4::stop() {
	TIMSK4 &= ~(_BV(TOIE4));
}

void AtMegaTimer4::_overflow() {
	count += 1;
	
	if (count >= msecs && !overflowing) {
		overflowing = 1;
		count = 0;
		func();
		overflowing = 0;
	}
}

ISR(_VECTOR(45)) {
	TCNT4H = atmegaTimer4.tcnt4 >> 8;
	TCNT4L = atmegaTimer4.tcnt4 & 0xff;
	TIFR4 |= _BV(TOV4);
	atmegaTimer4._overflow();
}

void AtMegaTimer5::set(unsigned long ms, void (*f)()) {
	float prescaler = 0.0;
	
	TIFR5 = 0xff;
	TIMSK5 = 0;
	TCCR5A &= ~((1<<WGM51) | (1<<WGM50));
	TCCR5B &= ~((1<<WGM52) | (1 << WGM53));
	if (F_CPU < 1000000UL) {	// prescaler set to 8
		TCCR5B &= ~((1<<CS52) | (1<<CS50));
		TCCR5B |= (1<<CS51);
		prescaler = 8.0;
	}
	else {  //if ((F_CPU >= 1000000UL) && (F_CPU <= 16000000UL)) prescaler set to 64
		TCCR5B &= ~(1<<CS52);
		TCCR5B |= (1<<CS51) | (1<<CS50);
		prescaler = 64.0;
	} 	
	tcnt5 = 65536U - (int)((float)F_CPU * 0.001 / prescaler);
	if(ms == 0) {
		msecs = 1;
	}
	else {
		msecs = ms;
	}
	func = f;
}

void AtMegaTimer5::setMicros(unsigned long us, void (*f)()) {
	float prescaler = 0.0;
	
	TIMSK5 = 0;
	TCCR5A &= ~((1<<WGM51) | (1<<WGM50));
	TCCR5B &= ~((1<<WGM52) | (1 << WGM53));
	//prescaler set to 1
	TCCR5B &= ~((1<<CS52) | (1<<CS51));
	TCCR5B |= (1<<CS50);
	prescaler = 1.0;
	if(us > 4095u)
		us = 4095u;
	tcnt5 = 65536U - (int)((float)F_CPU * 0.000001 * us / prescaler);
	msecs = 1;
	func = f;
}

void AtMegaTimer5::start() {
	count = 0;
	overflowing = 0;

	TCNT5H = tcnt5 >> 8;
	TCNT5L = tcnt5 & 0xff;
	TIFR5 |= _BV(TOV5);
	TIMSK5 |= _BV(TOIE5);
}

void AtMegaTimer5::stop() {
	TIMSK5 &= ~(_BV(TOIE5));
}

void AtMegaTimer5::_overflow() {
	count += 1;
	
	if (count >= msecs && !overflowing) {
		overflowing = 1;
		count = 0;
		func();
		overflowing = 0;
	}
}

ISR(_VECTOR(50)) {
	TCNT5H = atmegaTimer5.tcnt5 >> 8;
	TCNT5L = atmegaTimer5.tcnt5 & 0xff;
	TIFR5 |= _BV(TOV5);
	atmegaTimer5._overflow();
}

AtMegaTimer4 atmegaTimer4;
AtMegaTimer5 atmegaTimer5;

#endif

AtMegaTimer1 atmegaTimer1;
AtMegaTimer2 atmegaTimer2;
AtMegaTimer3 atmegaTimer3;

#endif
