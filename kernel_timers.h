
#ifndef TIMER_H_
#define TIMER_H_

#include <Arduino.h>

#include "list.h"

#define TICK							5
#define JIFFY							100
#define HZ                200

#define TIMER_MAGIC	0x4b87ad6e

struct timer_list {
	struct list_head entry;
	unsigned long expires;
	unsigned long magic;
	void (*function)(unsigned long);
	unsigned long data;
};

int timer_init(int timer);
void init_timer(struct timer_list * timer);
void setup_timer(struct timer_list *timer, void (*function)(unsigned long), unsigned long data );
int add_timer(struct timer_list* timer);
void mod_timer(struct timer_list* timer, unsigned long expires);
int del_timer(struct timer_list* timer);

int run_timers(void);

extern volatile unsigned long jiffies;
extern volatile unsigned long time_second;
extern volatile unsigned long clock_second;

#endif

