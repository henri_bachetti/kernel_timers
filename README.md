# KERNEL TIMERS

Kernel Timers is a timer library for ARDUINO, ESP8266 and ESP32. It provides an interface like the LINUX kernel timers.

This interface is well known in the LINUX world and has a very low memory footprint.

in the past I developed the same interface on MSP430.

### INTERFACE

**int timer_init(int timer);**

Initialises the timer interrupt system.
On AVR platforms, *timer* represents the hardware timer number :

 * 1 or 2 for an ATMAGA328P (ARDUINO UNO, NANO, MINI)
 * 1 to 5 for an ATMEGA2560 (ARDUINO MEGA)

On ESP8266 or ESP32 platforms it is ignored.

**void init_timer(struct timer_list * timer);**

Initializes a timer.

A timer must be declared like this :
```
struct timer_list timer;
```

**void setup_timer(struct timer_list *timer, void (*function)(unsigned long), unsigned long data );**

Initializes the *timer* and sets the user-provided callback function and context. Otherwise, the user can set these values (function and data) in the timer and simply call init_timer. Note that init_timer is called internally by setup_timer.

**int add_timer(struct timer_list* timer);**

add_timer inserts the *timer* into a sorted list, which is then polled 200 times per second.

**void mod_timer(struct timer_list* timer, unsigned long expires);**

mod_timer changes the time at which a *timer* expires. After the call, the new expires value will be used.

**int del_timer(struct timer_list* timer);**

If a *timer* needs to be removed from the list, del_timer should be called.

A typical example :
```
#include <kernel_timers.h>

#define TIMEOUT          (HZ*5)

struct timer_list timer;

void setup()
{
  Serial.begin(115200);
  // initialize timers system with TIMER1
  timer_init(1);
  init_timer(&timer);
  add_timer(&timer);
  Serial.println("\nStart the timer for 5 seconds");
  mod_timer(&timer, jiffies + TIMEOUT);
}

void loop()
{
  if (timer.expires != 0 && timer.expires == jiffies) {
    Serial.println("Timer has elapsed");
    mod_timer(&timer, 0);
  }
}
```
HZ represents the frequency of timers polling.

jiffies represents the current time.

So jiffies + (5 * HZ) represents the current time + 5 seconds.

The original kernel timer system was based on incrementing a kernel-internal value (jiffies) every timer interrupt. The timer interrupt becomes the default scheduling quantum, and all other timers are based on jiffies. The timer interrupt rate (and jiffy increment rate) is defined by a compile-time constant called HZ. Different platforms use different values for HZ. Historically, the kernel used 100 as the value for HZ, yielding a jiffy interval of 10 ms. With 2.4, the HZ value for i386 was changed to 1000, yeilding a jiffy interval of 1 ms. Recently (2.6.13) the kernel changed HZ for i386 to 250. (1000 was deemed too high).

This library uses a value of 200. This gives a 5ms timer resolution.

### INSTALLATION

Just unzip the archive in the folder named "libraries" in your Arduino folder.

### EXAMPLES

The library provides some examples :

|Example                    |Demo                                       |
|---------------------------|-------------------------------------------|
|timer_callback             |A demo of timer callbacks                  |
|timer_polling              |A demo of timer polling                    |

The examples are usable on the UNO, NANO, MINI, MEGA, ESP8266 and ESP32 platforms.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/03/arduino-esp8266-et-esp32-une-librairie.html

