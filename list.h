
#ifndef LIST_H_
#define _LIST_H_

#define LIST_POISON1  ((struct list_head *) 0x0010)
#define LIST_POISON2  ((struct list_head *) 0x0020)

struct list_head {
	struct list_head *next, *prev;
};

#define LIST_HEAD_INIT(name) { &(name), &(name) }

#define LIST_HEAD(name) \
	struct list_head name = LIST_HEAD_INIT(name)

#define INIT_LIST_HEAD(ptr) do { \
	(ptr)->next = (ptr); (ptr)->prev = (ptr); \
} while (0)

#define list_entry(ptr, type, member) \
			((type *)((char *)(ptr)-(unsigned long)(&((type *)0)->member)))

#define list_for_each(pos, head) \
	for (pos = (head)->next ; pos->next, pos != (head) ; pos = pos->next)

void list_add(struct list_head *list, struct list_head *head);
void list_add_tail(struct list_head *list, struct list_head *head);
void list_del(struct list_head *entry);
int list_empty(const struct list_head *head);

#endif /* LIST_H_ */
