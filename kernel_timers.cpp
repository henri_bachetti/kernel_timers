

#ifdef __AVR__
#include "atmega_timers.h"
#elif defined ESP8266
#include "user_interface.h"
#endif
#include "kernel_timers.h"

volatile unsigned long jiffies;

LIST_HEAD(head);

#define TIMER_TICK                100000L             // time base = 100ms
#define TIMER_TICK_MS             (TIMER_TICK/1000)

int timer_mscount;

#if defined ESP8266
os_timer_t myTimer;
#elif defined  ESP32
hw_timer_t *hw_timer = NULL;
#endif

#if defined ESP8266
void manageTimers(void *pArg)
#else
void manageTimers(void)
#endif
{
  timer_mscount++;
  if (timer_mscount == TICK) {
    run_timers();
    timer_mscount = 0;
  }
}

int timer_init(int timer)
{
#ifdef __AVR__
	switch (timer) {
		case 1:
			atmegaTimer1.set(TIMER_TICK_MS, manageTimers);
			atmegaTimer1.start();
			break;
		case 2:
			atmegaTimer2.set(TIMER_TICK_MS, manageTimers);
			atmegaTimer2.start();
			break;
#ifdef __AVR_ATmega2560__
		case 3:
			atmegaTimer3.set(TIMER_TICK_MS, manageTimers);
			atmegaTimer3.start();
			break;
		case 4:
			atmegaTimer4.set(TIMER_TICK_MS, manageTimers);
			atmegaTimer4.start();
			break;
		case 5:
			atmegaTimer5.set(TIMER_TICK_MS, manageTimers);
			atmegaTimer5.start();
			break;
		default:
			return -1;
#else
		default:
			return -1;
#endif
}
#elif defined ESP8266
	os_timer_setfn(&myTimer, manageTimers, 0);
	os_timer_arm(&myTimer, 100, true);
#elif defined  ESP32
  hw_timer = timerBegin(0, 80, true);
  timerAttachInterrupt(hw_timer, &manageTimers, true);
  timerAlarmWrite(hw_timer, TIMER_TICK_MS*1000, true);
	timerAlarmEnable(hw_timer);
#endif
  return 0;
}

void init_timer(struct timer_list * timer)
{
  if (timer->magic != TIMER_MAGIC) {
    memset(timer, 0, sizeof(struct timer_list));
    timer->magic = TIMER_MAGIC;
  }
}

void setup_timer(struct timer_list *timer, void (*function)(unsigned long), unsigned long data )
{
	init_timer(timer);
	timer->function = function;
	timer->data = data;
}

int add_timer(struct timer_list *timer)
{
	struct timer_list *t;
	struct list_head *p;

	for (p = head.next ; p != &head ; p = p->next) {
    	t = list_entry(p, struct timer_list, entry);
    	if (t == timer) {
    		return 0;
    	}
    }
	list_add_tail(&timer->entry, &head);
	return 0;
}

void mod_timer(struct timer_list* timer, unsigned long expires)
{
	timer->expires = expires;
}

int del_timer(struct timer_list* timer)
{
	list_del(&timer->entry);
	return 0;
}

int run_timers(void)
{
	register struct timer_list *timer;
	register struct list_head *p;
	int event = 0;

	jiffies += JIFFY;
	for (p = head.next ; p != &head ; p = p->next) {
		timer = list_entry(p, struct timer_list, entry);
			if (timer->expires != 0 && (timer->expires > jiffies - JIFFY && timer->expires <= jiffies)) {
				event = 1;
				if (timer->function) {
					(*timer->function)(timer->data);
				}
		}
	}
	return event;
}

